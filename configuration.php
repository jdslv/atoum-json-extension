<?php

use atoum\atoum\configurator;
use atoum\atoum\runner;
use atoum\atoum\scripts;

if (defined(scripts\runner::class)) {
    scripts\runner::addConfigurationCallable(function (configurator $script, runner $runner) {
        $runner->addExtension(new atoum\atoum\json\extension($script));
    });
}
