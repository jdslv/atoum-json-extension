<?php

declare(strict_types=1);

namespace atoum\atoum\json\interfaces;

interface asserter
{
    public function __get($asserter);
}
