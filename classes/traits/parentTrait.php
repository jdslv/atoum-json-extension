<?php

declare(strict_types=1);

namespace atoum\atoum\json\traits;

use atoum\atoum\exceptions;
use atoum\atoum\json\interfaces;

trait parentTrait
{
    protected ?interfaces\asserter $parent = null;

    public function __call($method, $arguments)
    {
        if ($this->parent) {
            return $this->parent->{$method}(...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    public function __get($asserter)
    {
        if (method_exists($this, $asserter)) {
            return $this->{$asserter}();
        }

        if ($this->parent) {
            return $this->parent->__get($asserter);
        }

        return parent::__get($asserter);
    }

    public function parent(): interfaces\asserter
    {
        return $this->parentIsSet()->parent;
    }

    protected function parentIsSet(string $message = 'Parent asserter is undefined'): self
    {
        if ($this->parent === null) {
            throw new exceptions\logic($message);
        }

        return $this;
    }

    public function setParent(interfaces\asserter $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
