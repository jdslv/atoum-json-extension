<?php

declare(strict_types=1);

namespace atoum\atoum\json\traits;

use atoum\atoum\json\interfaces\asserter;
use closure;

trait invokableTrait
{
    public function __invoke(closure $callback): asserter
    {
        $callback($this);

        return $this->parent ?? $this;
    }
}
