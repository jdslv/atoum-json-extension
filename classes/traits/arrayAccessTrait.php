<?php

declare(strict_types=1);

namespace atoum\atoum\json\traits;

use atoum\atoum\exceptions\logic;
use atoum\atoum\json\asserters;

trait arrayAccessTrait
{
    protected ?string $offsetClass = null;

    public function findOffsetClass(string $asserter): bool
    {
        $lower = strtolower($asserter);

        switch ($lower) {
            case 'bool':
            case 'boolean':
                $this->setOffsetClass(asserters\boolean::class);

                return true;

            case 'int':
            case 'integer':
                $this->setOffsetClass(asserters\integer::class);

                return true;

            case 'array':
            case 'float':
            case 'string':
                $class = 'atoum\atoum\json\asserters\php' . ucfirst($lower);

                $this->setOffsetClass($class);

                return true;

            case 'object':
                $this->setOffsetClass(asserters\json::class);

                return true;

            case 'variable':
                $this->setOffsetClass(asserters\variable::class);

                return true;

            default:
                return false;
        }
    }

    public function getOffsetClass(): string
    {
        $class = $this->offsetClass ?? asserters\variable::class;
        $this->offsetClass = null;

        return $class;
    }

    public function offsetExists(mixed $key): bool
    {
        $this->valueIsSet();

        if (is_object($this->value)) {
            return property_exists($this->value, (string) $key);
        }

        if (is_array($this->value)) {
            return array_key_exists($key, $this->value);
        }

        return false;
    }

    public function offsetGet(mixed $key): mixed
    {
        if ($this->offsetExists($key)) {
            $data = [];

            if (is_object($this->value)) {
                array_push($data, $this->value->{$key});
            }

            if (is_array($this->value)) {
                array_push($data, $this->value[$key]);
            }

            return $this->pass()->generator->__call($this->getOffsetClass(), $data)->setParent($this);
        }

        if ($this->parent) {
            return $this->parent->setOffsetClass($this->getOffsetClass())->offsetGet($key);
        }

        return $this->fail($this->_('"%s" has no key "%s"', json_encode($this->value), $key));
    }

    public function offsetSet(mixed $key, mixed $value): void
    {
        throw new logic('Not implemented');
    }

    public function offsetUnset(mixed $key): void
    {
        throw new logic('Not implemented');
    }

    public function setOffsetClass(?string $class = null): self
    {
        $this->offsetClass = $class;

        return $this;
    }
}
