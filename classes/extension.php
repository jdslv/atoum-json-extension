<?php

declare(strict_types=1);

namespace atoum\atoum\json;

use atoum\atoum;
use atoum\atoum\configurator;
use atoum\atoum\observable;
use atoum\atoum\runner;
use atoum\atoum\test;

class extension implements atoum\extension
{
    public function __construct(?configurator $configurator = null)
    {
        if ($configurator) {
            $parser = $configurator->getScript()->getArgumentsParser();

            $handler = function ($script, $argument, $values) {
                $script->getRunner()->addTestsFromDirectory(dirname(__DIR__) . '/tests/');
            };

            $parser->addHandler($handler, ['--test-ext']);
        }
    }

    public function handleEvent($event, observable $observable): void {}

    public function setRunner(runner $runner): self
    {
        return $this;
    }

    public function setTest(test $test): self
    {
        $handler = function ($data = null, $depth = null, $options = null) use ($test) {
            $asserter = new asserters\json($test->getAsserterGenerator());

            return $asserter->setWithTest($test)->setWith($data, $depth, $options);
        };

        $test->getAssertionManager()->setHandler('json', $handler);

        return $this;
    }
}
