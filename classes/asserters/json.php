<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use atoum\atoum\adapter;
use atoum\atoum\asserter\generator;
use atoum\atoum\json\asserters\integer as integerAsserter;
use atoum\atoum\locale;
use atoum\atoum\tools\variable\analyzer;
use exception;
use stdClass;

/**
 * Assertion dedicated to JSON.
 */
class json extends variable
{
    protected ?adapter $adapter = null;
    protected ?string $json = null;

    public function __call($method, $arguments)
    {
        $name = strtolower($method);

        if (substr($name, 0, 5) === 'isnot') {
            return $this->isNot(substr($name, 5), ...$arguments);
        }

        if (substr($name, 0, 2) === 'is') {
            return $this->is(substr($name, 2), ...$arguments);
        }

        return parent::__call($method, $arguments);
    }

    public function __construct(
        ?generator $generator = null,
        ?analyzer $analyzer = null,
        ?locale $locale = null,
        ?adapter $adapter = null,
    ) {
        parent::__construct($generator, $analyzer, $locale);

        $this->setAdapter($adapter);
    }

    public function __get($asserter)
    {
        if ($this->findOffsetClass($asserter)) {
            return $this;
        }

        switch (strtolower($asserter)) {
            case 'isarray':
            case 'isbool':
            case 'isboolean':
            case 'isfloat':
            case 'isinteger':
            case 'isnotarray':
            case 'isnotbool':
            case 'isnotboolean':
            case 'isnotfloat':
            case 'isnotinteger':
            case 'isnotnull':
            case 'isnotobject':
            case 'isnotstring':
            case 'isnull':
            case 'isobject':
            case 'isstring':
            case 'size':
                return $this->{$asserter}();

            default:
                return parent::__get($asserter);
        }
    }

    public function getAdapter(): adapter
    {
        return $this->adapter;
    }

    protected function getIsData(string $type): array
    {
        $lower = strtolower($type);
        $method = 'is_' . $lower;
        $message = 'a ' . $lower;

        if ($lower === 'bool' || $lower === 'boolean') {
            $method = 'is_bool';
            $message = 'a boolean';
        }

        if ($lower === 'null') {
            $message = 'null';
        }

        if ($this->adapter->in_array($lower, ['array', 'integer', 'object'], true)) {
            $message = 'an ' . $lower;
        }

        return [$method, $message];
    }

    /**
     * Checks that a property exists with a given name.
     *
     * Works only with object based JSON.
     */
    public function hasKey(string $key, ?string $message = null): self
    {
        if ($this->is('object', $message)) {
            if (!$this->offsetExists($key)) {
                return $this->fail($message ?? $this->_('"%s" has no key "%s"', $this->json, $key));
            }
        }

        return $this->pass();
    }

    /**
     * Checks that properties exists with given names.
     *
     * Works only with object based JSON.
     */
    public function hasKeys(array $keys, ?string $message = null): self
    {
        if ($this->valueIsSet($message)->is('object', $message)) {
            $properties = $this->adapter->get_object_vars($this->value);
            $diff = $this->adapter->array_diff($keys, $this->adapter->array_keys($properties));

            if ($diff) {
                return $this->fail($message ?? $this->_('"%s" has no keys "%s"', $this->json, implode('", "', $diff)));
            }
        }

        return $this->pass();
    }

    /**
     * Checks the size of the JSON.
     *
     * Works only on array based JSON.
     */
    public function hasSize(int $count, ?string $message = null): self
    {
        if ($this->is('array', $message)) {
            $nb = $this->adapter->count($this->value);

            if ($nb !== $count) {
                return $this->fail($message ?? $this->_('"%s" has size of %d', $this->json, $nb));
            }
        }

        return $this->pass();
    }

    /**
     * Indicates if the JSON is a given type.
     */
    public function is(string $type, ?string $message = null): self
    {
        [$method, $tmp] = $this->valueIsSet($message)->getIsData($type);
        $defaulMessage = '"%s" is not ' . $tmp;

        if (!$this->adapter->{$method}($this->value)) {
            return $this->fail($message ?? $this->_($defaulMessage, $this->json));
        }

        return $this->pass();
    }

    /**
     * Indicates if the JSON is not a given type.
     */
    public function isNot(string $type, ?string $message = null): self
    {
        [$method, $tmp] = $this->valueIsSet($message)->getIsData($type);
        $defaulMessage = '"%s" is ' . $tmp;

        if ($this->adapter->{$method}($this->value)) {
            return $this->fail($message ?? $this->_($defaulMessage, $this->json));
        }

        return $this->pass();
    }

    /**
     * Indicates if the JSON is not null.
     *
     * @param ?string $message
     */
    public function isNotNull($message = null): self
    {
        return $this->isNot('null', $message);
    }

    /**
     * Indicates if the JSON is null.
     *
     * @param ?string $message
     */
    public function isNull($message = null): self
    {
        return $this->is('null', $message);
    }

    /**
     * Checks that a property does not exist with a given name.
     *
     * Works only with object based JSON.
     */
    public function notHasKey(string $key, ?string $message = null): self
    {
        if ($this->is('object', $message)) {
            if ($this->offsetExists($key)) {
                return $this->fail($message ?? $this->_('"%s" has key "%s"', $this->json, $key));
            }
        }

        return $this->pass();
    }

    /**
     * Checks that properties does not exist with given names.
     *
     * Works only with object based JSON.
     */
    public function notHasKeys(array $keys, ?string $message = null): self
    {
        if ($this->valueIsSet($message)->is('object', $message)) {
            $properties = $this->adapter->get_object_vars($this->value);
            $diff = $this->adapter->array_intersect($keys, $this->adapter->array_keys($properties));

            if ($diff) {
                return $this->fail($message ?? $this->_('"%s" has keys "%s"', $this->json, implode('", "', $diff)));
            }
        }

        return $this->pass();
    }

    /**
     * Checks the size of the JSON.
     *
     * Works only on array based JSON.
     */
    public function notHasSize(int $count, ?string $message = null): self
    {
        if ($this->is('array', $message)) {
            $nb = $this->adapter->count($this->value);

            if ($nb === $count) {
                return $this->fail($message ?? $this->_('"%s" has size of %d', $this->json, $nb));
            }
        }

        return $this->pass();
    }

    public function setAdapter(?adapter $adapter = null): self
    {
        $this->adapter = $adapter ?? new adapter();

        return $this;
    }

    public function setWith($value): self
    {
        if ($value === null) {
            return $this->fail('JSON is undefined');
        }

        if ($value instanceof stdClass) {
            $value = json_encode($value);
        }

        if (!$this->adapter->is_string($value)) {
            return $this->fail($this->_('"%s" is not a valid JSON', $value));
        }

        try {
            $data = $this->adapter->json_decode($value, false, 512, JSON_THROW_ON_ERROR);
        } catch (exception $error) {
            return $this->fail($this->_('"%s" is not a valid JSON', $value));
        }

        parent::setWith($data);
        $this->json = $value;

        return $this->pass();
    }

    /**
     * Return an `integer` asserter based on array size.
     *
     * Works only on array based JSON.
     */
    public function size(): integerAsserter
    {
        return $this->is('array')->generator->__call(integerAsserter::class, [count($this->value)])->setParent($this);
    }

    protected function valueIsSet($message = null): self
    {
        return parent::valueIsSet($message ?? 'JSON is undefined');
    }
}
