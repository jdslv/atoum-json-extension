<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use atoum\atoum\asserters;
use atoum\atoum\json\interfaces;
use atoum\atoum\json\traits\invokableTrait;
use atoum\atoum\json\traits\parentTrait;

/**
 * Asserter dedicated to floats.
 *
 * Overrided to allow our asserter traversing.
 */
class phpFloat extends asserters\phpFloat implements interfaces\asserter
{
    use invokableTrait;
    use parentTrait;
}
