<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use atoum\atoum\asserters;
use atoum\atoum\json\interfaces;
use atoum\atoum\json\traits\invokableTrait;
use atoum\atoum\json\traits\parentTrait;

/**
 * Asserter dedicated to strings.
 *
 * Overrided to allow our asserter traversing.
 */
class phpString extends asserters\phpString implements interfaces\asserter
{
    use invokableTrait;
    use parentTrait;
}
