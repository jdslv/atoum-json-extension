<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use arrayAccess;
use atoum\atoum\asserters;
use atoum\atoum\json\interfaces;
use atoum\atoum\json\traits\arrayAccessTrait;
use atoum\atoum\json\traits\invokableTrait;
use atoum\atoum\json\traits\parentTrait;

/**
 * Asserter dedicated to variables.
 *
 * Overrided to allow our asserter traversing.
 */
class variable extends asserters\variable implements arrayAccess, interfaces\asserter
{
    use arrayAccessTrait;
    use invokableTrait;
    use parentTrait;
}
