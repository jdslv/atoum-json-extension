<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use atoum\atoum\asserters;
use atoum\atoum\json\interfaces;
use atoum\atoum\json\traits\invokableTrait;
use atoum\atoum\json\traits\parentTrait;

/**
 * Asserter dedicated to booleans.
 *
 * Overrided to allow our asserter traversing.
 */
class boolean extends asserters\boolean implements interfaces\asserter
{
    use invokableTrait;
    use parentTrait;
}
