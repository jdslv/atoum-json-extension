<?php

declare(strict_types=1);

namespace atoum\atoum\json\asserters;

use atoum\atoum\asserters;
use atoum\atoum\json\interfaces;
use atoum\atoum\json\traits\arrayAccessTrait;
use atoum\atoum\json\traits\invokableTrait;
use atoum\atoum\json\traits\parentTrait;

/**
 * Asserter dedicated to arrays.
 *
 * Overrided to allow our asserter traversing.
 */
class phpArray extends asserters\phpArray implements interfaces\asserter
{
    use arrayAccessTrait;
    use invokableTrait;
    use parentTrait;

    public function __get($asserter)
    {
        if (method_exists($this, $asserter)) {
            return $this->{$asserter}();
        }

        if ($this->findOffsetClass($asserter)) {
            return $this;
        }

        if ($this->parent) {
            return $this->parent->__get($asserter);
        }

        return parent::__get($asserter);
    }

    protected function valueIsSet($message = null): self
    {
        return parent::valueIsSet($message ?? 'JSON is undefined');
    }
}
