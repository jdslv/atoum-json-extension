
# jdslv/atoum-json-extension

> The atoum json extension allows you to make assertions on JSON

[![GitLab](https://img.shields.io/static/v1?message=GitLab&logo=gitlab&color=grey&label=)](https://gitlab.com/jdslv/atoum-json-extension)
[![Latest stable version](https://img.shields.io/packagist/v/jdslv/atoum-json-extension)](https://packagist.org/packages/jdslv/atoum-json-extension)
[![Build status](https://gitlab.com/jdslv/atoum-json-extension/badges/main/pipeline.svg)](https://gitlab.com/jdslv/atoum-json-extension/-/pipelines)
[![Coverage status](https://img.shields.io/codecov/c/gitlab/jdslv/atoum-json-extension)](https://codecov.io/gl/jdslv/atoum-json-extension/)
[![Minimal PHP version](https://img.shields.io/packagist/php-v/jdslv/atoum-json-extension)](https://gitlab.com/jdslv/atoum-json-extension)
[![License](https://img.shields.io/packagist/l/jdslv/atoum-json-extension)](https://gitlab.com/jdslv/atoum-json-extension/-/blob/main/LICENSE)


This atoum extension allows you to test JSON using [atoum](https://github.com/atoum/atoum).


## Install it

Install extension using [composer](https://getcomposer.org):

```
composer require --dev jdslv/atoum-json-extension
```


## Use it

The extension is automatically added to atoum configuration.

You can use it directly after installation.

```php
<?php // tests example

namespace my\project\tests\unit;

use atoum;

class MyClass extends atoum\atoum\test
{
    public function testJSON()
    {
        $this
            ->if($json = '<<<JSON
{
    "name": "jdslv/atoum-json-extension",
    "keywords": [
        "TDD",
        "atoum",
        "test",
        "unit testing",
        "json",
        "extension",
        "atoum-extension"
    ],
}
JSON')
            ->then
                ->json($json)
                    ->isObject
                    ->hasKeys(['keywords', 'name'])

                    ->array['keywords']
                        ->contains('atoum')
                        ->contains('json')

                    ->string['name']
                        ->isIdenticalTo('jdslv/atoum-json-extension')
        ;
    }
}
```


## Asserters

Every asserters allow a final `string` argument to personalize error message.

They are all fluent, you can chain assertions, we automatically find the better context for your asserters.

You should also know that all assertions without parameter can be written with or without parenthesis.
So `$this->integer(0)->isZero()` is the same as `$this->integer(0)->isZero`.


### json

It's the assertion dedicated to JSON.

#### hasKey

`hasKey` checks that a property exists with a given name.

Works only with object based JSON.

```
public function hasKey(string $key, ?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';

$this
    ->json($json1)
        ->hasKey('foo')  // succeed
        ->hasKey('bar')  // failed
    ->json($json2)
        ->hasKey(0)      // failed
;
```

#### hasKeys

`hasKeys` checks that properties exists with given names.

Works only with object based JSON.

```
public function hasKeys(array $keys, ?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';

$this
    ->json($json1)
        ->hasKeys(['foo'])         // succeed
        ->hasKeys(['bar'])         // failed
        ->hasKeys(['foo', 'bar'])  // failed
    ->json($json2)
        ->hasKeys([0])             // failed
;
```

#### hasSize

`hasSize` checks the size of the JSON.

Works only on array based JSON.

```
public function hasSize(int $count, ?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->hasSize(2)  // succeed
        ->hasSize(3)  // failed
    ->json($json2)
        ->hasSize(1)  // failed
    ->json($json3)
        ->hasSize(1)  // failed
;
```

#### is

`is` indicates if the JSON is a given type.

```
public function is(string $type, ?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->is('null')     // failed
        ->is('boolean')  // failed
        ->is('integer')  // failed
        ->is('float')    // failed
        ->is('string')   // failed
        ->is('array')    // succeed
        ->is('object')   // failed
    ->json($json2)
        ->is('null')     // failed
        ->is('boolean')  // failed
        ->is('integer')  // failed
        ->is('float')    // failed
        ->is('string')   // succeed
        ->is('array')    // failed
        ->is('object')   // failed
    ->json($json3)
        ->is('null')     // failed
        ->is('boolean')  // failed
        ->is('integer')  // failed
        ->is('float')    // failed
        ->is('string')   // failed
        ->is('array')    // failed
        ->is('object')   // succeed
;
```

#### isArray

`isArray` indicates if the JSON is an array.

```
public function isArray(?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isArray()  // succeed
    ->json($json2)
        ->isArray()  // failed
    ->json($json3)
        ->isArray()  // failed
;
```

#### isBool

`isBool` indicates if the JSON is a boolean.

```
public function isBool(?string $message = null): self
```

```php
<?php

$json1 = 'true';
$json2 = 'false';
$json3 = 'null';
$json4 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isBool()  // succeed
    ->json($json2)
        ->isBool()  // succeed
    ->json($json3)
        ->isBool()  // failed
    ->json($json4)
        ->isBool()  // failed
;
```

#### isBoolean

`isBoolean` indicates if the JSON is not a boolean.

```
public function isBoolean(?string $message = null): self
```

```php
<?php

$json1 = 'true';
$json2 = 'false';
$json3 = 'null';
$json4 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isBoolean()  // succeed
    ->json($json2)
        ->isBoolean()  // succeed
    ->json($json3)
        ->isBoolean()  // failed
    ->json($json4)
        ->isBoolean()  // failed
;
```

#### isEqualTo

```
public function isEqualTo($value, ?string $message = null): self
```

> `isEqualTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isEqualTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-equal-to).


#### isFloat

`isFloat` indicates if the JSON is a float.

```
public function isFloat(?string $message = null): self
```

```php
<?php

$json1 = '3.14';
$json2 = '123';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isFloat()  // succeed
    ->json($json2)
        ->isFloat()  // failed
    ->json($json3)
        ->isFloat()  // failed
;
```

#### isIdenticalTo

```
public function isIdenticalTo($value, ?string $message = null): self
```

> `isIdenticalTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isIdenticalTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-identical-to).


#### isInteger

`isInteger` indicates if the JSON is an integer.

```
public function isInteger(?string $message = null): self
```

```php
<?php

$json1 = '123';
$json2 = '3.14';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isInteger()  // succeed
    ->json($json2)
        ->isInteger()  // failed
    ->json($json3)
        ->isInteger()  // failed
;
```

#### isNot

`isNot` indicates if the JSON is not a given type.

```
public function isNot(string $type, ?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNot('null')     // succeed
        ->isNot('boolean')  // succeed
        ->isNot('integer')  // succeed
        ->isNot('float')    // succeed
        ->isNot('string')   // succeed
        ->isNot('array')    // failed
        ->isNot('object')   // succeed
    ->json($json2)
        ->isNot('null')     // succeed
        ->isNot('boolean')  // succeed
        ->isNot('integer')  // succeed
        ->isNot('float')    // succeed
        ->isNot('string')   // failed
        ->isNot('array')    // succeed
        ->isNot('object')   // succeed
    ->json($json3)
        ->isNot('null')     // succeed
        ->isNot('boolean')  // succeed
        ->isNot('integer')  // succeed
        ->isNot('float')    // succeed
        ->isNot('string')   // succeed
        ->isNot('array')    // succeed
        ->isNot('object')   // failed
;
```

#### isNotArray

`isNotArray` indicates if the JSON is not an array.

```
public function isNotArray(?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotArray()  // failed
    ->json($json2)
        ->isNotArray()  // succeed
    ->json($json3)
        ->isNotArray()  // succeed
;
```

#### isNotBool

`isNotBool` indicates if the JSON is not a boolean.

```
public function isNotBool(?string $message = null): self
```

```php
<?php

$json1 = 'true';
$json2 = 'false';
$json3 = 'null';
$json4 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotBool()  // failed
    ->json($json2)
        ->isNotBool()  // failed
    ->json($json3)
        ->isNotBool()  // succeed
    ->json($json4)
        ->isNotBool()  // succeed
;
```

#### isNotBoolean

`isNotBoolean` indicates if the JSON is not a boolean.

```
public function isNotBoolean(?string $message = null): self
```

```php
<?php

$json1 = 'true';
$json2 = 'false';
$json3 = 'null';
$json4 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotBoolean()  // failed
    ->json($json2)
        ->isNotBoolean()  // failed
    ->json($json3)
        ->isNotBoolean()  // succeed
    ->json($json4)
        ->isNotBoolean()  // succeed
;
```

#### isNotEqualTo

```
public function isNotEqualTo($value, ?string $message = null): self
```

> `isNotEqualTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isNotEqualTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-equal-to).


#### isNotFloat

`isNotFloat` indicates if the JSON is not a float.

```
public function isNotFloat(?string $message = null): self
```

```php
<?php

$json1 = '3.14';
$json2 = '123';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotFloat()  // failed
    ->json($json2)
        ->isNotFloat()  // succeed
    ->json($json3)
        ->isNotFloat()  // succeed
;
```

#### isNotIdenticalTo

```
public function isNotIdenticalTo($value, ?string $message = null): self
```

> `isNotIdenticalTo` is a method inherited from the `variable` asserter.
For more information, refer to the documentation of [variable::isNotIdenticalTo](https://atoum.readthedocs.io/en/latest/asserters.html#variable-is-identical-to).


#### isNotInteger

`isNotInteger` indicates if the JSON is not an integer.

```
public function isNotInteger(?string $message = null): self
```

```php
<?php

$json1 = '123';
$json2 = '3.14';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotInteger()  // failed
    ->json($json2)
        ->isNotInteger()  // succeed
    ->json($json3)
        ->isNotInteger()  // succeed
;
```

#### isNotNull

`isNotNull` indicates if the JSON is not null.

```
public function isNotNull(?string $message = null): self
```

```php
<?php

$json1 = 'null';
$json2 = '3.14';
$json3 = '["foo", 123]';

$this
    ->json($json1)
        ->isNotNull()  // failed
    ->json($json2)
        ->isNotNull()  // succeed
    ->json($json3)
        ->isNotNull()  // succeed
;
```

#### isNotObject

`isNotObject` indicates if the JSON is not an object.

```
public function isNotObject(?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';
$json3 = '123';

$this
    ->json($json1)
        ->isNotObject()  // failed
    ->json($json2)
        ->isNotObject()  // succeed
    ->json($json3)
        ->isNotObject()  // succeed
;
```

#### isNotString

`isNotString` indicates if the JSON is not a string.

```
public function isNotString(?string $message = null): self
```

```php
<?php

$json1 = '"foo"';
$json2 = '["foo", 123]';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isNotString()  // failed
    ->json($json2)
        ->isNotString()  // succeed
    ->json($json3)
        ->isNotString()  // succeed
;
```

#### isNull

`isNull` indicates if the JSON is null.

```
public function isNull(?string $message = null): self
```

```php
<?php

$json1 = 'null';
$json2 = '3.14';
$json3 = '["foo", 123]';

$this
    ->json($json1)
        ->isNull()  // succeed
    ->json($json2)
        ->isNull()  // failed
    ->json($json3)
        ->isNull()  // failed
;
```

#### isObject

`isObject` indicates if the JSON is an object.

```
public function isObject(?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';
$json3 = '123';

$this
    ->json($json1)
        ->isObject()  // succeed
    ->json($json2)
        ->isObject()  // failed
    ->json($json3)
        ->isObject()  // failed
;
```

#### isString

`isString` indicates if the JSON is a string.

```
public function isString(?string $message = null): self
```

```php
<?php

$json1 = '"foo"';
$json2 = '["foo", 123]';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->isString()  // succeed
    ->json($json2)
        ->isString()  // failed
    ->json($json3)
        ->isString()  // failed
;
```

#### notHasKey

`notHasKey` checks that a property does not exist with a given name.

Works only with object based JSON.

```
public function notHasKey(string $key, ?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';

$this
    ->json($json1)
        ->notHasKey('foo')  // failed
        ->notHasKey('bar')  // succeed
    ->json($json2)
        ->notHasKey(0)      // failed
;
```

#### notHasKeys

`notHasKeys` checks that properties does not exist with given names.

Works only with object based JSON.

```
public function notHasKeys(array $keys, ?string $message = null): self
```

```php
<?php

$json1 = '{"foo": "bar"}';
$json2 = '["foo", 123]';

$this
    ->json($json1)
        ->notHasKeys(['foo'])         // failed
        ->notHasKeys(['bar'])         // succeed
        ->notHasKeys(['foo', 'bar'])  // failed
    ->json($json2)
        ->notHasKeys([0])             // failed
;
```

#### notHasSize

`notHasSize` checks the size of the JSON.

Works only on array based JSON.

```
public function notHasSize(int $count, ?string $message = null): self
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->notHasSize(2)  // failed
        ->notHasSize(3)  // succeed
    ->json($json2)
        ->notHasSize(1)  // failed
    ->json($json3)
        ->notHasSize(1)  // failed
;
```

#### size

`size` return an `integer` asserter based on array size.

Works only on array based JSON.

```
public function size(): atoum\atoum\json\asserters\integer
```

```php
<?php

$json1 = '["foo", 123]';
$json2 = '"foo"';
$json3 = '{"foo": "bar"}';

$this
    ->json($json1)
        ->size()
            ->isEqualTo(2)  // succeed
            ->isEqualTo(3)  // failed
    ->json($json2)
        ->size()            // failed
    ->json($json3)
        ->size()            // failed
;
```


## License

`jdslv/atoum-json-extension` is released under the MIT License.
See the bundled [LICENSE](LICENSE) file for details.

![atoum](http://atoum.org/images/logo/atoum.png)
