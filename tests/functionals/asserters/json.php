<?php

namespace atoum\atoum\json\tests\functionals\asserters;

use atoum\atoum\json\tests\functionals;

class json extends functionals\test
{
    public function testExampleFromWikipedia()
    {
        $this
            ->json($this->resource('wikipedia.json'))
                ->isObject

                ->string['firstName']
                    ->isIdenticalTo('John')

                ->string['lastName']
                    ->isIdenticalTo('Smith')

                ->boolean['isAlive']
                    ->isTrue

                ->integer['age']
                    ->isIdenticalTo(27)

                ->object['address']
                    ->string['streetAddress']->isIdenticalTo('21 2nd Street')
                    ->string['city']->isIdenticalTo('New York')
                    ->string['state']->isIdenticalTo('NY')
                    ->string['postalCode']->isIdenticalTo('10021-3100')

                ->array['phoneNumbers'](function ($child) {
                    $child
                        ->hasSize(2)
                        ->object[0]
                            ->string['type']->isIdenticalTo('home')
                            ->string['number']->isIdenticalTo('212 555-1234')
                        ->object[1]
                            ->string['type']->isIdenticalTo('office')
                            ->string['number']->isIdenticalTo('646 555-4567')
                    ;
                })

                ->array['children']
                    ->isEmpty

                ->variable['spouse']
                    ->isNull
        ;
    }
}
