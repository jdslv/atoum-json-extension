<?php

declare(strict_types=1);

namespace atoum\atoum\json\tests\units;

use atoum;
use closure;
use mock;

class extension extends atoum\atoum\test
{
    public function test__construct()
    {
        $this
            ->given($parser = new mock\atoum\atoum\script\arguments\parser)

            ->if($runner = new mock\atoum\atoum\runner)

            ->if($script = new atoum\atoum\scripts\runner(uniqid()))
            ->and($script->setArgumentsParser($parser))
            ->and($script->setRunner($runner))

            ->if($configurator = new mock\atoum\atoum\configurator($script))

            ->if($dirname = dirname(__DIR__) . '/')
            ->then
                ->assert('Without configurator')
                    ->if($this->newTestedInstance)
                    ->then
                        ->mock($parser)
                            ->wasNotCalled

                        ->mock($runner)
                            ->wasNotCalled

                ->assert('With configurator')
                    ->if($this->newTestedInstance($configurator))
                    ->then
                        ->mock($parser)
                            ->call('addHandler')
                                ->once

                        ->mock($runner)
                            ->call('addTestsFromDirectory')
                                ->never

                        // what comes next is an artificial way to achieve 100% coverage
                        ->array($parser->getHandlers())
                            ->hasKey('--test-ext')
                            ->child['--test-ext'](function ($child) use ($script) {
                                $child
                                    ->hasSize(1)
                                    ->hasKey(0)
                                    ->object[0]
                                        ->isInstanceOf(closure::class)
                                ;

                                $child->getValue()[0]->__invoke($script, uniqid(), uniqid());
                            })

                        ->mock($runner)
                            ->call('addTestsFromDirectory')
                                ->withIdenticalArguments($dirname)
                                    ->once
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->hasInterface(atoum\atoum\extension::class)
        ;
    }

    public function testHandleEvent()
    {
        $this
            ->given($observable = new mock\atoum\atoum\observable)
            ->and($this->resetMock($observable))

            ->if($event = new mock\stdClass)
            ->and($this->resetMock($event))

            ->then
                ->variable($this->newTestedInstance->handleEvent($event, $observable))
                    ->isNull

                ->mock($event)->wasNotCalled
                ->mock($observable)->wasNotCalled
        ;
    }

    public function testSetRunner()
    {
        $this
            ->given($runner = new mock\atoum\atoum\runner)
            ->and($this->resetMock($runner))
            ->then
                ->object($this->newTestedInstance->setRunner($runner))
                    ->isTestedInstance

                ->mock($runner)->wasNotCalled
        ;
    }

    public function testSetTest()
    {
        $this
            ->assert('Add handler')
                ->if($this->newTestedInstance)
                ->and($manager = new mock\atoum\atoum\test\assertion\manager)
                ->and($test = new mock\atoum\atoum\test)
                ->and($test->setAssertionManager($manager))
                ->then
                    ->object($this->testedInstance->setTest($test))
                        ->isTestedInstance

                    ->mock($manager)
                        ->call('setHandler')
                            ->withArguments('json')->once

            ->assert('Without value')
                ->given($test = new static)

                ->if($this->newTestedInstance->setTest($test))
                ->then
                    ->exception(function () use ($test) {
                        $test->json;
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage('JSON is undefined')

            ->assert('With value')
                ->given($test = new static)
                ->and($json = json_encode([uniqid() => uniqid()]))

                ->if($this->newTestedInstance->setTest($test))
                ->then
                    ->object($test->json($json))
                        ->isInstanceOf(atoum\atoum\json\asserters\json::class)
        ;
    }
}
