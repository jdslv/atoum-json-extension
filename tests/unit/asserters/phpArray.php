<?php

declare(strict_types=1);

namespace atoum\atoum\json\tests\units\asserters;

use atoum;
use Generator;
use mock;

class phpArray extends atoum\atoum\test
{
    public function arrayProvider()
    {
        yield [[uniqid(), rand()]];

        yield [[uniqid() => uniqid(), uniqid() => rand()]];
    }

    /**
     * @return Generator<string, bool, class-string>
     */
    public function offsetClassProvider(): Generator
    {
        // key, result, class

        yield ['array', true, atoum\atoum\json\asserters\phpArray::class];

        yield ['bool', true, atoum\atoum\json\asserters\boolean::class];

        yield ['boolean', true, atoum\atoum\json\asserters\boolean::class];

        yield ['float', true, atoum\atoum\json\asserters\phpFloat::class];

        yield ['int', true, atoum\atoum\json\asserters\integer::class];

        yield ['integer', true, atoum\atoum\json\asserters\integer::class];

        yield ['string', true, atoum\atoum\json\asserters\phpString::class];

        yield ['variable', true, atoum\atoum\json\asserters\variable::class];

        yield ['object', true, atoum\atoum\json\asserters\json::class];

        yield [uniqid(), false, atoum\atoum\json\asserters\variable::class];
    }

    public function test__call()
    {
        $this
            ->assert('Without parent')
                ->if($method = uniqid())
                ->then
                    ->exception(function () use ($method) {
                        $this->newTestedInstance->{$method}();
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

            ->assert('With parent')
                ->given($method = uniqid())
                ->and($args = [uniqid(), uniqid()])
                ->and($parent = new mock\atoum\atoum\json\asserters\variable)

                ->if($this->newTestedInstance->setParent($parent))
                ->then
                    ->exception(function () use ($method, $args) {
                        $this->testedInstance->{$method}(...$args);
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

                    ->mock($parent)
                        ->call('__call')
                            ->withIdenticalArguments($method, $args)->once
        ;
    }

    public function test__get()
    {
        $this
            ->assert('Without parent')
                ->if($asserter = uniqid())
                ->then
                    ->exception(function () use ($asserter) {
                        $this->newTestedInstance->{$asserter};
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

            ->assert('With parent')
                ->given($asserter = uniqid())
                ->and($parent = new mock\atoum\atoum\json\asserters\variable)

                ->if($this->newTestedInstance->setParent($parent))
                ->then
                    ->exception(function () use ($asserter) {
                        $this->testedInstance->{$asserter};
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

                    ->mock($parent)
                        ->call('__get')
                            ->withIdenticalArguments($asserter)->once
        ;

        foreach ($this->offsetClassProvider() as $value) {
            [$key, $result, $class] = $value;

            if ($result) {
                $this
                    ->string($this->newTestedInstance->getOffsetClass())
                        ->isIdenticalTo(atoum\atoum\json\asserters\variable::class)

                    ->object($this->testedInstance->{$key})
                        ->isTestedInstance

                    ->string($this->testedInstance->getOffsetClass())
                        ->isIdenticalTo($class)
                ;
            } else {
                $this
                    ->if($asserter = uniqid())
                    ->then
                        ->string($this->newTestedInstance->getOffsetClass())
                            ->isIdenticalTo(atoum\atoum\json\asserters\variable::class)

                        ->exception(function () use ($asserter) {
                            $this->testedInstance->{$asserter};
                        })
                            ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                            ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

                        ->string($this->testedInstance->getOffsetClass())
                            ->isIdenticalTo(atoum\atoum\json\asserters\variable::class)
                ;
            }
        }
    }

    public function test__invoke()
    {
        $this
            ->assert('Without parent')
                ->given($key = uniqid())
                ->and($value = uniqid())
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith([$key => $value]))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isTestedInstance

                    ->boolean($done)
                        ->isTrue

            ->assert('Without parent')
                ->given($value = rand(1, 10) > 5)
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith([$key => $value]))

                ->if($parent = new mock\atoum\atoum\json\interfaces\asserter)
                ->and($this->testedInstance->setParent($parent))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isIdenticalTo($parent)

                    ->boolean($done)
                        ->isTrue
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\asserters\phpArray::class)
                ->hasInterface(atoum\atoum\json\interfaces\asserter::class)
        ;
    }

    /**
     * @dataProvider offsetClassProvider
     *
     * @param class-string $class
     */
    public function testFindOffsetClass(string $key, bool $result, string $class)
    {
        $this
            ->string($this->newTestedInstance->getOffsetClass())
                ->isIdenticalTo(atoum\atoum\json\asserters\variable::class)

            ->boolean($this->testedInstance->findOffsetClass($key))
                ->isIdenticalTo($result)

            ->string($this->testedInstance->getOffsetClass())
                ->isIdenticalTo($class)
        ;
    }

    /**
     * @dataProvider arrayProvider
     */
    public function testOffsetExists(array $data)
    {
        $this
            ->assert('json::offsetExists(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->offsetExists(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->if($json = json_encode($data))
            ->and($key = array_rand($data))
            ->and($badKey = rand())
            ->then
                ->assert(sprintf('json::offsetExists("%s") with "%s"', $key, $json))
                    ->if($this->newTestedInstance->setWith($data))
                    ->then
                        ->boolean($this->testedInstance->offsetExists($key))
                            ->isTrue

                ->assert(sprintf('json::offsetExists("%s") with "%s"', $badKey, $json))
                    ->if($this->newTestedInstance->setWith($data))
                    ->then
                        ->boolean($this->testedInstance->offsetExists($badKey))
                            ->isFalse
        ;
    }

    /**
     * @dataProvider arrayProvider
     */
    public function testOffsetGet(array $data)
    {
        $this
            ->assert('json::offsetGet(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->offsetGet(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->if($json = json_encode($data))
            ->and($key = array_rand($data))
            ->and($badKey = rand())
            ->then
                ->assert(sprintf('json::offsetGet("%s") with "%s"', $key, $json))
                    ->if($this->newTestedInstance->setWith($data))
                    ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($value = $this->testedInstance->offsetGet($key))
                            ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                        ->variable($value->getValue())
                            ->isIdenticalTo($data[$key])

                ->assert(sprintf('json::offsetGet("%s") with "%s"', $badKey, $json))
                    ->if($this->newTestedInstance->setWith($data))
                    ->and($this->testedInstance->setWithTest(new static)) // initialize a new test with an empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($badKey) {
                            $this->testedInstance->offsetGet($badKey);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" has no key "%d"', $json, $badKey))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::offsetGet(%d) with "%s" and parent with it', count($data), $json))
                    ->given($parentTest = new static)
                    ->and($currentTest = new static)

                    ->given($newValue = uniqid())
                    ->and($parent = $this->newTestedInstance)
                    ->and($parent->setWith(array_values(array_merge($data, [$newValue]))))
                    ->and($parent->setWithTest($parentTest))

                    ->if($this->newTestedInstance->setWith($data))
                    ->and($this->testedInstance->setWithTest($currentTest))
                    ->and($this->testedInstance->setParent($parent))
                    ->then
                        ->object($value = $this->testedInstance->offsetGet(count($data)))
                            ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                        ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                        ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                        ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                        ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(1)

                        ->variable($value->getValue())
                            ->isIdenticalTo($newValue)

                ->assert(sprintf('json::offsetGet(%d) with "%s" and parent without it', $badKey, $json))
                    ->given($parentTest = new static)
                    ->and($currentTest = new static)

                    ->if($newData = [uniqid(), uniqid()])

                    ->given($parent = $this->newTestedInstance)
                    ->and($parent->setWith($newData))
                    ->and($parent->setWithTest($parentTest))

                    ->if($this->newTestedInstance->setWith($data))
                    ->and($this->testedInstance->setWithTest($currentTest))
                    ->and($this->testedInstance->setParent($parent))
                    ->then
                        ->exception(function () use ($badKey) {
                            $this->testedInstance->offsetGet($badKey);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" has no key "%d"', json_encode($newData), $badKey))

                        ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                        ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                        ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                        ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(0)
        ;
    }

    public function testOffsetSet()
    {
        $this
            ->exception(function () {
                $this->newTestedInstance->offsetSet(uniqid(), uniqid());
            })
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Not implemented')
        ;
    }

    public function testOffsetUnset()
    {
        $this
            ->exception(function () {
                $this->newTestedInstance->offsetUnset(uniqid());
            })
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Not implemented')
        ;
    }

    public function testParent_SetParent()
    {
        $this
            ->assert('No parent defined')
                ->exception(function () {
                    $this->newTestedInstance->parent();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('Parent asserter is undefined')

            ->assert('Set parent')
                ->if($parent = new mock\atoum\atoum\json\asserters\variable)
                ->then
                    ->object($this->newTestedInstance->setParent($parent))
                        ->isTestedInstance

                    ->object($this->testedInstance->parent())
                        ->isIdenticalTo($this->testedInstance->parent)
                        ->isNotTestedInstance
                        ->isIdenticalTo($parent)
        ;
    }
}
