<?php

declare(strict_types=1);

namespace atoum\atoum\json\tests\units\asserters;

use arrayAccess;
use atoum;
use Generator;
use mock;

class json extends atoum\atoum\test
{
    /**
     * @return Generator<string, array|bool|float|integer|string, string, string, class-string>
     */
    public function arrayAccessProvider(): Generator
    {
        // key, value, access, assertion, instance

        $key = uniqid();

        yield [uniqid(), uniqid(), 'string', 'string', atoum\atoum\json\asserters\phpString::class];

        yield [uniqid(), rand(), 'int', 'integer', atoum\atoum\json\asserters\integer::class];

        yield [uniqid(), rand(), 'integer', 'integer', atoum\atoum\json\asserters\integer::class];

        yield [uniqid(), rand() / 10000, 'float', 'float', atoum\atoum\json\asserters\phpFloat::class];

        yield [uniqid(), true, 'bool', 'boolean', atoum\atoum\json\asserters\boolean::class];

        yield [uniqid(), false, 'bool', 'boolean', atoum\atoum\json\asserters\boolean::class];

        yield [uniqid(), true, 'boolean', 'boolean', atoum\atoum\json\asserters\boolean::class];

        yield [uniqid(), false, 'boolean', 'boolean', atoum\atoum\json\asserters\boolean::class];

        yield [uniqid(), null, 'variable', 'variable', atoum\atoum\json\asserters\variable::class];

        yield [uniqid(), [uniqid(), rand()], 'array', 'array', atoum\atoum\json\asserters\phpArray::class];
    }

    /**
     * @return Generator<string, string>
     */
    public function jsonProvider(): Generator
    {
        yield [json_encode(uniqid()), 'string'];

        yield [json_encode(rand()), 'integer'];

        yield [json_encode(rand() / 10000), 'float'];

        yield [json_encode(true), 'bool'];

        yield [json_encode(false), 'bool'];

        yield [json_encode(null), 'null'];

        yield [json_encode([uniqid(), rand()]), 'array'];

        yield [json_encode([uniqid() => uniqid(), uniqid() => uniqid()]), 'object'];
    }

    /**
     * @return Generator<string, bool, class-string>
     */
    public function offsetClassProvider(): Generator
    {
        // key, result, class

        yield ['array', true, atoum\atoum\json\asserters\phpArray::class];

        yield ['bool', true, atoum\atoum\json\asserters\boolean::class];

        yield ['boolean', true, atoum\atoum\json\asserters\boolean::class];

        yield ['float', true, atoum\atoum\json\asserters\phpFloat::class];

        yield ['int', true, atoum\atoum\json\asserters\integer::class];

        yield ['integer', true, atoum\atoum\json\asserters\integer::class];

        yield ['string', true, atoum\atoum\json\asserters\phpString::class];

        yield ['variable', true, atoum\atoum\json\asserters\variable::class];

        yield ['object', true, atoum\atoum\json\asserters\json::class];

        yield [uniqid(), false, atoum\atoum\json\asserters\variable::class];
    }

    public function test__call()
    {
        $this
            ->if($method = uniqid())
            ->then
                ->exception(function () use ($method) {
                    $this->newTestedInstance->{$method}();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))
        ;
    }

    public function test__get()
    {
        $this
            ->if($asserter = uniqid())
            ->then
                ->exception(function () use ($asserter) {
                    $this->newTestedInstance->{$asserter};
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                    ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))
        ;
    }

    public function test__invoke()
    {
        $this
            ->assert('Without parent')
                ->given($key = uniqid())
                ->and($value = uniqid())
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith((object) [$key => $value]))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isTestedInstance

                    ->boolean($done)
                        ->isTrue

            ->assert('Without parent')
                ->given($key = uniqid())
                ->and($value = uniqid())
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith((object) [$key => $value]))

                ->if($parent = new mock\atoum\atoum\json\interfaces\asserter)
                ->and($this->testedInstance->setParent($parent))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isIdenticalTo($parent)

                    ->boolean($done)
                        ->isTrue
        ;
    }

    /**
     * @dataProvider arrayAccessProvider
     *
     * @param array|bool|float|integer|string $value
     * @param class-string $instance
     */
    public function testArrayAccess(string $key, $value, string $access, string $assertion, string $instance)
    {
        $this
            ->if($this->newTestedInstance->setWith(json_encode([$key => $value])))
            ->then
                ->object($obj = $this->testedInstance->{$access}[$key])
                    ->isInstanceOf($instance)

                ->object($obj->parent())
                    ->isTestedInstance

                ->{$assertion}($obj->getValue())
                    ->isIdenticalTo($value)
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\json\asserters\variable::class)
                ->hasInterface(atoum\atoum\json\interfaces\asserter::class)
                ->hasInterface(arrayAccess::class)
        ;
    }

    /**
     * @dataProvider offsetClassProvider
     *
     * @param class-string $class
     */
    public function testFindOffsetClass(string $key, bool $result, string $class)
    {
        $this
            ->string($this->newTestedInstance->getOffsetClass())
                ->isIdenticalTo(atoum\atoum\json\asserters\variable::class)

            ->boolean($this->testedInstance->findOffsetClass($key))
                ->isIdenticalTo($result)

            ->string($this->testedInstance->getOffsetClass())
                ->isIdenticalTo($class)
        ;
    }

    public function testGetAdapter_SetAdapter()
    {
        $this
            ->given($adapter = new mock\atoum\atoum\adapter)
            ->if($this->newTestedInstance)
            ->then
                ->object($this->testedInstance->getAdapter())
                    ->isInstanceOf(atoum\atoum\adapter::class)

                ->object($this->testedInstance->setAdapter($adapter))
                    ->isTestedInstance

                ->object($this->testedInstance->getAdapter())
                    ->isInstanceOf(atoum\atoum\adapter::class)
                    ->isIdenticalTo($adapter)
        ;
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testHasKey_NotHasKey(string $json, string $type)
    {
        $this
            ->assert('json::hasKey(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->hasKey(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::hasKey(`string`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->hasKey(uniqid(), $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::notHasKey(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->notHasKey(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::notHasKey(`string`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->notHasKey(uniqid(), $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)
        ;

        if ($type === 'object') {
            $this
                ->if($key = array_rand(json_decode($json, true)))
                ->and($badKey = uniqid())
                ->then
                    ->assert(sprintf('json::hasKey("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasKey($key))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::hasKey("%s", $message) with "%s"', $key, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasKey($key, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::hasKey("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKey) {
                                $this->testedInstance->hasKey($badKey);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no key "%s"', $json, $badKey))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasKey("%s", $message) with "%s"', $badKey, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKey, $message) {
                                $this->testedInstance->hasKey($badKey, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasKey("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasKey($badKey))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasKey("%s", $message) with "%s"', $badKey, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasKey($badKey, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasKey("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($key) {
                                $this->testedInstance->notHasKey($key);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has key "%s"', $json, $key))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasKey("%s", $message) with "%s"', $key, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($key, $message) {
                                $this->testedInstance->notHasKey($key, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        } else {
            $this
                ->assert(sprintf('json::hasKey(`string`) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->hasKey(uniqid());
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::hasKey(`string`, $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->hasKey(uniqid(), $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasKey(`string`) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->notHasKey(uniqid());
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasKey(`string`, $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->notHasKey(uniqid(), $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testHasKeys_NotHasKeys(string $json, string $type)
    {
        $this
            ->assert('json::hasKeys(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->hasKeys([uniqid()]);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::hasKeys(`string`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->hasKeys([uniqid()], $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::notHasKeys(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->notHasKeys([uniqid()]);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::notHasKeys(`string`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->notHasKeys([uniqid()], $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)
        ;

        if ($type === 'object') {
            $this
                ->if($keys = array_keys(json_decode($json, true)))
                ->and($partialKeys = [uniqid(), array_rand(json_decode($json, true))])
                ->and($badKeys = [uniqid()])
                ->then
                    ->assert(sprintf('json::hasKeys(["%s"]) with "%s"', implode('", "', $keys), $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasKeys($keys))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::hasKeys(["%s"], $message) with "%s"', implode('", "', $keys), $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasKeys($keys, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::hasKeys(["%s"]) with "%s"', implode('", "', $partialKeys), $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($partialKeys) {
                                $this->testedInstance->hasKeys($partialKeys);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no keys "%s"', $json, $partialKeys[0]))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasKeys(["%s"], $message) with "%s"', implode('", "', $partialKeys), $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($partialKeys, $message) {
                                $this->testedInstance->hasKeys($partialKeys, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasKeys(["%s"]) with "%s"', implode('", "', $badKeys), $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKeys) {
                                $this->testedInstance->hasKeys($badKeys);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no keys "%s"', $json, implode('", "', $badKeys)))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasKeys(["%s"], $message) with "%s"', implode('", "', $badKeys), $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKeys, $message) {
                                $this->testedInstance->hasKeys($badKeys, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasKeys(["%s"]) with "%s"', implode('", "', $badKeys), $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasKeys($badKeys))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasKeys(["%s"], $message) with "%s"', implode('", "', $badKeys), $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasKeys($badKeys, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasKeys(["%s"]) with "%s"', implode('", "', $keys), $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($keys) {
                                $this->testedInstance->notHasKeys($keys);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has keys "%s"', $json, implode('", "', $keys)))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasKeys(["%s"], $message) with "%s"', implode('", "', $keys), $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($keys, $message) {
                                $this->testedInstance->notHasKeys($keys, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        } else {
            $this
                ->assert(sprintf('json::hasKeys([`string`]) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->hasKeys([uniqid()]);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::hasKeys([`string`], $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->hasKeys([uniqid()], $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasKeys([`string`]) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->notHasKeys([uniqid()]);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasKeys([`string`], $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->notHasKeys([uniqid()], $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testHasSize_NotHasSize(string $json, string $type)
    {
        $this
            ->assert('json::hasSize(`number`) without value')
                ->exception(function () {
                    $this->newTestedInstance->hasSize(rand());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::hasSize(`number`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->hasSize(rand(), $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::notHasSize(`number`) without value')
                ->exception(function () {
                    $this->newTestedInstance->notHasSize(rand());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::notHasSize(`number`, $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->notHasSize(rand(), $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)
        ;

        if ($type === 'array') {
            $this
                ->if($count = count(json_decode($json)))
                ->and($badCount = rand() + $count)
                ->then
                    ->assert(sprintf('json::hasSize(`%d`) with "%s"', $badCount, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badCount) {
                                $this->testedInstance->hasSize($badCount);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has size of %d', $json, $count))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasSize(`%d`, $message) with "%s"', $badCount, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badCount, $message) {
                                $this->testedInstance->hasSize($badCount, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::hasSize(%d) with "%s"', $count, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasSize($count))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::hasSize(%d, $message) with "%s"', $count, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->hasSize($count, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasSize(%d) with "%s"', $count, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($count) {
                                $this->testedInstance->notHasSize($count);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has size of %d', $json, $count))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasSize(%d, $message) with "%s"', $count, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($count, $message) {
                                $this->testedInstance->notHasSize($count, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasSize(%d) with "%s"', $count, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($count) {
                                $this->testedInstance->notHasSize($count);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has size of %d', $json, $count))

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasSize(%d, $message) with "%s"', $count, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($count, $message) {
                                $this->testedInstance->notHasSize($count, $message);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage($message)

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                    ->assert(sprintf('json::notHasSize(%d) with "%s"', $count, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasSize($badCount))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)

                    ->assert(sprintf('json::notHasSize(%d, $message) with "%s"', $count, $json))
                        ->given($message = uniqid())

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($this->testedInstance->notHasSize($badCount, $message))
                                ->isTestedInstance

                            ->integer($score->getAssertionNumber())->isEqualTo(2)
                            ->integer($score->getPassNumber())->isEqualTo(2)
            ;
        } else {
            $this
                ->assert(sprintf('json::hasSize(`number`) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->hasSize(rand());
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::hasSize(`number`, $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->hasSize(rand(), $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasSize(`number`) with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->notHasSize(rand());
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::notHasSize(`number`, $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->notHasSize(rand(), $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsArray_IsNotArray(string $json, string $type)
    {
        $this
            ->assert('json::is("array") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('array');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("array", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('array', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("array") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('array');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("array", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('array', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isArray() without value')
                ->exception(function () {
                    $this->newTestedInstance->isArray();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isArray($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isArray($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isArray without value')
                ->exception(function () {
                    $this->newTestedInstance->isArray;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotArray() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotArray();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotArray($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotArray($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotArray without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotArray;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'array') {
            $this
                ->assert(sprintf('json::is("array") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('array'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("array", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('array', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("array") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('array');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("array", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('array', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isArray() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isArray())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isArray($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isArray($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isArray with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isArray)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotArray() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotArray();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotArray($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotArray($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotArray with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotArray;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("array") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('array');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("array", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('array', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("array") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('array'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("array", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('array', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isArray() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isArray();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isArray($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isArray($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isArray with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isArray;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotArray() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotArray())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotArray($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotArray($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotArray with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotArray)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsBool_IsNotBool(string $json, string $type)
    {
        $this
            ->assert('json::is("bool") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('bool');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("bool", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('bool', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("bool") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('bool');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("bool", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('bool', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isBool() without value')
                ->exception(function () {
                    $this->newTestedInstance->isBool();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isBool($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isBool($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isBool without value')
                ->exception(function () {
                    $this->newTestedInstance->isBool;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotBool() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotBool();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotBool($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotBool($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotBool without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotBool;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'bool') {
            $this
                ->assert(sprintf('json::is("bool") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('bool'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("bool", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('bool', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("bool") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('bool');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("bool", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('bool', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBool() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBool())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBool($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBool($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBool with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBool)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBool() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotBool();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBool($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotBool($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBool with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotBool;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("bool") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('bool');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("bool", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('bool', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("bool") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('bool'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("bool", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('bool', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBool() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isBool();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBool($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isBool($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBool with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isBool;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBool() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBool())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBool($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBool($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBool with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBool)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsBoolean_IsNotBoolean(string $json, string $type)
    {
        $this
            ->assert('json::is("boolean") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('boolean');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("boolean", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('boolean', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("boolean") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('boolean');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("boolean", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('boolean', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isBoolean() without value')
                ->exception(function () {
                    $this->newTestedInstance->isBoolean();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isBoolean($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isBoolean($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isBoolean without value')
                ->exception(function () {
                    $this->newTestedInstance->isBoolean;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotBoolean() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotBoolean();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotBoolean($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotBoolean($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotBoolean without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotBoolean;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'bool') {
            $this
                ->assert(sprintf('json::is("boolean") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('boolean'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("boolean", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('boolean', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("boolean") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('boolean');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("boolean", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('boolean', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBoolean() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBoolean())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBoolean($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBoolean($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBoolean with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isBoolean)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBoolean() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotBoolean();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBoolean($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotBoolean($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBoolean with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotBoolean;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("boolean") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('boolean');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("boolean", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('boolean', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("boolean") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('boolean'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("boolean", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('boolean', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isBoolean() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isBoolean();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBoolean($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isBoolean($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isBoolean with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isBoolean;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a boolean', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotBoolean() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBoolean())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBoolean($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBoolean($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotBoolean with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotBoolean)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsFloat_IsNotFloat(string $json, string $type)
    {
        $this
            ->assert('json::is("float") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('float');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("float", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('float', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("float") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('float');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("float", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('float', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isFloat() without value')
                ->exception(function () {
                    $this->newTestedInstance->isFloat();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isFloat($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isFloat($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isFloat without value')
                ->exception(function () {
                    $this->newTestedInstance->isFloat;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotFloat() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotFloat();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotFloat($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotFloat($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotFloat without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotFloat;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'float') {
            $this
                ->assert(sprintf('json::is("float") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('float'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("float", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('float', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("float") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('float');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("float", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('float', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isFloat() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isFloat())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isFloat($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isFloat($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isFloat with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isFloat)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotFloat() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotFloat();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotFloat($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotFloat($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotFloat with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotFloat;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("float") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('float');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("float", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('float', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("float") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('float'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("float", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('float', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isFloat() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isFloat();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isFloat($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isFloat($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isFloat with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isFloat;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a float', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotFloat() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotFloat())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotFloat($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotFloat($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotFloat with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotFloat)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsInteger_IsNotInteger(string $json, string $type)
    {
        $this
            ->assert('json::is("integer") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('integer');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("integer", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('integer', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("integer") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('integer');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("integer", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('integer', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isInteger() without value')
                ->exception(function () {
                    $this->newTestedInstance->isInteger();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isInteger($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isInteger($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isInteger without value')
                ->exception(function () {
                    $this->newTestedInstance->isInteger;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotInteger() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotInteger();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotInteger($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotInteger($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotInteger without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotInteger;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'integer') {
            $this
                ->assert(sprintf('json::is("integer") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('integer'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("integer", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('integer', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("integer") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('integer');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("integer", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('integer', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isInteger() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isInteger())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isInteger($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isInteger($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isInteger with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isInteger)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotInteger() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotInteger();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotInteger($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotInteger($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotInteger with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotInteger;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("integer") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('integer');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("integer", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('integer', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("integer") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('integer'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("integer", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('integer', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isInteger() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isInteger();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isInteger($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isInteger($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isInteger with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isInteger;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an integer', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotInteger() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotInteger())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotInteger($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotInteger($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotInteger with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotInteger)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsNull_IsNotNull(string $json, string $type)
    {
        $this
            ->assert('json::is("null") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('null');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("null", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('null', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("null") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('null');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("null", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('null', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNull() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNull();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNull($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNull($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNull without value')
                ->exception(function () {
                    $this->newTestedInstance->isNull;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotNull() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotNull();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotNull($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotNull($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotNull without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotNull;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'null') {
            $this
                ->assert(sprintf('json::is("null") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('null'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("null", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('null', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("null") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('null');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("null", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('null', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNull() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNull())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNull($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNull($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNull with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNull)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotNull() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotNull();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotNull($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotNull($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotNull with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotNull;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("null") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('null');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("null", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('null', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("null") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('null'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("null", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('null', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNull() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNull();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNull($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNull($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNull with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNull;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not null', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotNull() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotNull())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotNull($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotNull($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotNull with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotNull)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsObject_IsNotObject(string $json, string $type)
    {
        $this
            ->assert('json::is("object") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('object');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("object", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('object', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("object") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('object');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("object", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('object', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isObject() without value')
                ->exception(function () {
                    $this->newTestedInstance->isObject();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isObject($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isObject($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isObject without value')
                ->exception(function () {
                    $this->newTestedInstance->isObject;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotObject() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotObject();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotObject($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotObject($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotObject without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotObject;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'object') {
            $this
                ->assert(sprintf('json::is("object") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('object'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("object", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('object', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("object") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('object');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("object", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('object', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isObject() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isObject())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isObject($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isObject($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isObject with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isObject)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotObject() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotObject();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotObject($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotObject($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotObject with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotObject;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("object") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('object');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("object", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('object', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("object") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('object'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("object", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('object', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isObject() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isObject();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isObject($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isObject($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isObject with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isObject;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an object', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotObject() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotObject())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotObject($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotObject($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotObject with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotObject)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testIsString_IsNotString(string $json, string $type)
    {
        $this
            ->assert('json::is("string") without value')
                ->exception(function () {
                    $this->newTestedInstance->is('string');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::is("string", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->is('string', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNot("string") without value')
                ->exception(function () {
                    $this->newTestedInstance->isNot('string');
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNot("string", $message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNot('string', $message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isString() without value')
                ->exception(function () {
                    $this->newTestedInstance->isString();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isString($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isString($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isString without value')
                ->exception(function () {
                    $this->newTestedInstance->isString;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotString() without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotString();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::isNotString($message) without value')
                ->if($message = uniqid())
                ->exception(function () use ($message) {
                    $this->newTestedInstance->isNotString($message);
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage($message)

            ->assert('json::isNotString without value')
                ->exception(function () {
                    $this->newTestedInstance->isNotString;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'string') {
            $this
                ->assert(sprintf('json::is("string") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('string'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::is("string", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->is('string', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("string") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNot('string');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("string", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNot('string', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isString() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isString())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isString($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isString($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isString with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isString)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotString() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotString();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotString($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isNotString($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotString with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isNotString;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::is("string") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->is('string');
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::is("string", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->is('string', $message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNot("string") with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('string'))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNot("string", $message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNot('string', $message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isString() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isString();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isString($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($message) {
                            $this->testedInstance->isString($message);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage($message)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isString with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->isString;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not a string', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::isNotString() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotString())
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotString($message) with "%s"', $json))
                    ->given($message = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotString($message))
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::isNotString with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->isNotString)
                            ->isTestedInstance

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testOffsetExists(string $json, string $type)
    {
        $this
            ->assert('json::offsetExists(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->offsetExists(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'object') {
            $this
                ->if($key = array_rand(json_decode($json, true)))
                ->and($badKey = uniqid())
                ->then
                    ->assert(sprintf('json::offsetExists("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->then
                            ->boolean($this->testedInstance->offsetExists($key))
                                ->isTrue

                    ->assert(sprintf('json::offsetExists("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->then
                            ->boolean($this->testedInstance->offsetExists($badKey))
                                ->isFalse
            ;
        } elseif ($type === 'array') {
            $this
                ->if($key = array_rand(json_decode($json, true)))
                ->and($badKey = rand() + $key)
                ->then
                    ->assert(sprintf('json::offsetExists("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->then
                            ->boolean($this->testedInstance->offsetExists($key))
                                ->isTrue

                    ->assert(sprintf('json::offsetExists("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->then
                            ->boolean($this->testedInstance->offsetExists($badKey))
                                ->isFalse
            ;
        } else {
            $this
                ->assert(sprintf('json::offsetExists(`string`) with "%s"', $json))
                    ->given($badKey = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->boolean($this->testedInstance->offsetExists($badKey))
                            ->isFalse

                        ->integer($score->getAssertionNumber())->isEqualTo(0)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testOffsetGet(string $json, string $type)
    {
        $this
            ->assert('json::offsetGet(`string`) without value')
                ->exception(function () {
                    $this->newTestedInstance->offsetGet(uniqid());
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'object') {
            $this
                ->if($data = json_decode($json, true))
                ->and($key = array_rand($data))
                ->and($badKey = uniqid())
                ->then
                    ->assert(sprintf('json::offsetGet("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($value = $this->testedInstance->offsetGet($key))
                                ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                            ->integer($score->getAssertionNumber())->isEqualTo(1)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                            ->variable($value->getValue())
                                ->isIdenticalTo($data[$key])

                    ->assert(sprintf('json::offsetGet("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKey) {
                                $this->testedInstance->offsetGet($badKey);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no key "%s"', $json, $badKey))

                            ->integer($score->getAssertionNumber())->isEqualTo(1)
                            ->integer($score->getPassNumber())->isEqualTo(0)

                    ->assert(sprintf('json::offsetGet("%s") with "%s" and parent with it', $badKey, $json))
                        ->given($parentTest = new static)
                        ->and($currentTest = new static)

                        ->given($newValue = uniqid())
                        ->and($parent = $this->newTestedInstance)
                        ->and($parent->setWith(json_encode([$badKey => $newValue])))
                        ->and($parent->setWithTest($parentTest))

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest($currentTest))
                        ->and($this->testedInstance->setParent($parent))
                        ->then
                            ->object($value = $this->testedInstance->offsetGet($badKey))
                                ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                            ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                            ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                            ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                            ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(1)

                            ->variable($value->getValue())
                                ->isIdenticalTo($newValue)

                    ->assert(sprintf('json::offsetGet("%s") with "%s" and parent without it', $badKey, $json))
                        ->given($parentTest = new static)
                        ->and($currentTest = new static)

                        ->if($newJson = json_encode([uniqid() => uniqid()]))

                        ->given($parent = $this->newTestedInstance)
                        ->and($parent->setWith($newJson))
                        ->and($parent->setWithTest($parentTest))

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest($currentTest))
                        ->and($this->testedInstance->setParent($parent))
                        ->then
                            ->exception(function () use ($badKey) {
                                $this->testedInstance->offsetGet($badKey);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no key "%s"', $newJson, $badKey))

                            ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                            ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                            ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                            ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(0)
            ;
        } elseif ($type === 'array') {
            $this
                ->if($data = json_decode($json, true))
                ->and($key = array_rand($data))
                ->and($badKey = rand() + $key)
                ->then
                    ->assert(sprintf('json::offsetGet("%s") with "%s"', $key, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->object($value = $this->testedInstance->offsetGet($key))
                                ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                            ->integer($score->getAssertionNumber())->isEqualTo(1)
                            ->integer($score->getPassNumber())->isEqualTo(1)

                            ->variable($value->getValue())
                                ->isIdenticalTo($data[$key])

                    ->assert(sprintf('json::offsetGet("%s") with "%s"', $badKey, $json))
                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest(new static)) // empty score
                        ->and($score = $this->testedInstance->getTest()->getScore())
                        ->then
                            ->exception(function () use ($badKey) {
                                $this->testedInstance->offsetGet($badKey);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no key "%d"', $json, $badKey))

                            ->integer($score->getAssertionNumber())->isEqualTo(1)
                            ->integer($score->getPassNumber())->isEqualTo(0)

                    ->assert(sprintf('json::offsetGet("%s") with "%s" and parent with it', $badKey, $json))
                        ->given($parentTest = new static)
                        ->and($currentTest = new static)

                        ->given($newValue = uniqid())
                        ->and($parent = $this->newTestedInstance)
                        ->and($parent->setWith(json_encode([$badKey => $newValue])))
                        ->and($parent->setWithTest($parentTest))

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest($currentTest))
                        ->and($this->testedInstance->setParent($parent))
                        ->then
                            ->object($value = $this->testedInstance->offsetGet($badKey))
                                ->isInstanceOf(atoum\atoum\json\asserters\variable::class)

                            ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                            ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                            ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                            ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(1)

                            ->variable($value->getValue())
                                ->isIdenticalTo($newValue)

                    ->assert(sprintf('json::offsetGet("%s") with "%s" and parent without it', $badKey, $json))
                        ->given($parentTest = new static)
                        ->and($currentTest = new static)

                        ->if($newJson = json_encode([uniqid(), uniqid()]))

                        ->given($parent = $this->newTestedInstance)
                        ->and($parent->setWith($newJson))
                        ->and($parent->setWithTest($parentTest))

                        ->if($this->newTestedInstance->setWith($json))
                        ->and($this->testedInstance->setWithTest($currentTest))
                        ->and($this->testedInstance->setParent($parent))
                        ->then
                            ->exception(function () use ($badKey) {
                                $this->testedInstance->offsetGet($badKey);
                            })
                                ->isInstanceOf(atoum\atoum\asserter\exception::class)
                                ->hasMessage(sprintf('"%s" has no key "%d"', $newJson, $badKey))

                            ->integer($currentTest->getScore()->getAssertionNumber())->isEqualTo(0)
                            ->integer($currentTest->getScore()->getPassNumber())->isEqualTo(0)

                            ->integer($parentTest->getScore()->getAssertionNumber())->isEqualTo(1)
                            ->integer($parentTest->getScore()->getPassNumber())->isEqualTo(0)
            ;
        } else {
            $this
                ->assert(sprintf('json::offsetGet(`string`) with "%s"', $json))
                    ->given($badKey = uniqid())

                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () use ($badKey) {
                            $this->testedInstance->offsetGet($badKey);
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" has no key "%s"', $json, $badKey))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }

    public function testOffsetSet()
    {
        $this
            ->exception(function () {
                $this->newTestedInstance->offsetSet(uniqid(), uniqid());
            })
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Not implemented')
        ;
    }

    public function testOffsetUnset()
    {
        $this
            ->exception(function () {
                $this->newTestedInstance->offsetUnset(uniqid());
            })
                ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                ->hasMessage('Not implemented')
        ;
    }

    public function testSetWith()
    {
        $this
            ->assert('Null value')
                ->exception(function () {
                    $this->newTestedInstance->setWith(null);
                })
                    ->isInstanceOf(atoum\atoum\asserter\exception::class)
                    ->hasMessage('JSON is undefined')

            ->assert('With not a JSON')
                ->given($json = random_int(0, PHP_INT_MAX))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($json) {
                        $this->testedInstance->setWith($json);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid JSON', $json))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With invalid JSON')
                ->given($json = '{ foo: "bar" }')

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->exception(function () use ($json) {
                        $this->testedInstance->setWith($json);
                    })
                        ->isInstanceOf(atoum\atoum\asserter\exception::class)
                        ->hasMessage(sprintf('"%s" is not a valid JSON', $json))

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(0)

            ->assert('With JSON string')
                ->given($json = json_encode(uniqid()))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON integer')
                ->given($json = json_encode(random_int(0, PHP_INT_MAX)))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON float')
                ->given($json = json_encode(random_int(0, PHP_INT_MAX) / 100))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON bool')
                ->given($json = json_encode(random_int(0, 1) ? true : false))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON null value')
                ->given($json = json_encode(null))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON array')
                ->given($json = json_encode([uniqid(), uniqid()]))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)

            ->assert('With JSON object')
                ->given($json = json_encode([uniqid() => uniqid()]))

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWithTest(new static)) // empty score
                ->and($score = $this->testedInstance->getTest()->getScore())
                ->then
                    ->object($this->testedInstance->setWith($json))
                        ->isTestedInstance

                    ->integer($score->getAssertionNumber())->isEqualTo(1)
                    ->integer($score->getPassNumber())->isEqualTo(1)
        ;
    }

    /**
     * @dataProvider jsonProvider
     */
    public function testSize(string $json, string $type)
    {
        $this
            ->assert('json::size() without value')
                ->exception(function () {
                    $this->newTestedInstance->size();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')

            ->assert('json::size without value')
                ->exception(function () {
                    $this->newTestedInstance->size;
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('JSON is undefined')
        ;

        if ($type === 'array') {
            $this
                ->assert(sprintf('json::size() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->size())
                            ->isInstanceOf(atoum\atoum\json\asserters\integer::class)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)

                ->assert(sprintf('json::size with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->object($this->testedInstance->size)
                            ->isInstanceOf(atoum\atoum\json\asserters\integer::class)

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(1)
            ;
        } else {
            $this
                ->assert(sprintf('json::size() with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->size();
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)

                ->assert(sprintf('json::size with "%s"', $json))
                    ->if($this->newTestedInstance->setWith($json))
                    ->and($this->testedInstance->setWithTest(new static)) // empty score
                    ->and($score = $this->testedInstance->getTest()->getScore())
                    ->then
                        ->exception(function () {
                            $this->testedInstance->size;
                        })
                            ->isInstanceOf(atoum\atoum\asserter\exception::class)
                            ->hasMessage(sprintf('"%s" is not an array', $json))

                        ->integer($score->getAssertionNumber())->isEqualTo(1)
                        ->integer($score->getPassNumber())->isEqualTo(0)
            ;
        }
    }
}
