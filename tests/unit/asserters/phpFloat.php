<?php

declare(strict_types=1);

namespace atoum\atoum\json\tests\units\asserters;

use atoum;
use mock;

class phpFloat extends atoum\atoum\test
{
    public function test__call()
    {
        $this
            ->assert('Without parent')
                ->if($method = uniqid())
                ->then
                    ->exception(function () use ($method) {
                        $this->newTestedInstance->{$method}();
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

            ->assert('With parent')
                ->given($method = uniqid())
                ->and($args = [uniqid(), uniqid()])
                ->and($parent = new mock\atoum\atoum\json\asserters\variable)

                ->if($this->newTestedInstance->setParent($parent))
                ->then
                    ->exception(function () use ($method, $args) {
                        $this->testedInstance->{$method}(...$args);
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $method))

                    ->mock($parent)
                        ->call('__call')
                            ->withIdenticalArguments($method, $args)->once
        ;
    }

    public function test__get()
    {
        $this
            ->assert('Without parent')
                ->if($asserter = uniqid())
                ->then
                    ->exception(function () use ($asserter) {
                        $this->newTestedInstance->{$asserter};
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

            ->assert('With parent')
                ->given($asserter = uniqid())
                ->and($parent = new mock\atoum\atoum\json\asserters\variable)

                ->if($this->newTestedInstance->setParent($parent))
                ->then
                    ->exception(function () use ($asserter) {
                        $this->testedInstance->{$asserter};
                    })
                        ->isInstanceOf(atoum\atoum\exceptions\logic\invalidArgument::class)
                        ->hasMessage(sprintf('Asserter \'%s\' does not exist', $asserter))

                    ->mock($parent)
                        ->call('__get')
                            ->withIdenticalArguments($asserter)->once
        ;
    }

    public function test__invoke()
    {
        $this
            ->assert('Without parent')
                ->given($value = rand() / rand(1, 10000))
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith($value))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isTestedInstance

                    ->boolean($done)
                        ->isTrue

            ->assert('Without parent')
                ->given($value = rand() / rand(1, 10000))
                ->and($done = false)
                ->and($closure = function ($child) use (&$done) {
                    $done = true;

                    $this
                        ->object($child)
                            ->isTestedInstance
                    ;
                })

                ->if($this->newTestedInstance)
                ->and($this->testedInstance->setWith($value))

                ->if($parent = new mock\atoum\atoum\json\interfaces\asserter)
                ->and($this->testedInstance->setParent($parent))
                ->then
                    ->object($this->testedInstance)
                        ->isCallable

                    ->object($this->testedInstance->__invoke($closure))
                        ->isIdenticalTo($parent)

                    ->boolean($done)
                        ->isTrue
        ;
    }

    public function testClass()
    {
        $this
            ->testedClass
                ->isSubClassOf(atoum\atoum\asserters\phpFloat::class)
                ->hasInterface(atoum\atoum\json\interfaces\asserter::class)
        ;
    }

    public function testParent_SetParent()
    {
        $this
            ->assert('No parent defined')
                ->exception(function () {
                    $this->newTestedInstance->parent();
                })
                    ->isInstanceOf(atoum\atoum\exceptions\logic::class)
                    ->hasMessage('Parent asserter is undefined')

            ->assert('Set parent')
                ->if($parent = new mock\atoum\atoum\json\asserters\variable)
                ->then
                    ->object($this->newTestedInstance->setParent($parent))
                        ->isTestedInstance

                    ->object($this->testedInstance->parent())
                        ->isIdenticalTo($this->testedInstance->parent)
                        ->isNotTestedInstance
                        ->isIdenticalTo($parent)
        ;
    }
}
