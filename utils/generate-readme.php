<?php

/*
Really dirty script to generate the "asserters" part of the README.

Dont judge me, I was tired :)
*/

require __DIR__ . '/../vendor/autoload.php';

define('VERBOSE', true);

$ns = 'atoum\atoum\json\asserters\\';
$longDescription = 'This atoum extension allows you to test JSON using [atoum](https://github.com/atoum/atoum).';

$data = json_decode(file_get_contents(__DIR__ . '/data.json'), true);
$package = json_decode(file_get_contents(__DIR__ . '/../composer.json'), true);

if ($handle = opendir(__DIR__ . '/../classes/asserters')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry !== '.' && $entry !== '..') {
            $classname = str_replace('.php', '', $entry);
            $cls = $ns . $classname;
            $reflection = new ReflectionClass($cls);
            $reflectionName = $reflection->getName();

            if (!array_key_exists($classname, $data)) {
                continue;
            }

            $asserter = &$data[$classname];

            if (!array_key_exists('fullname', $asserter)) {
                $asserter['fullname'] = $reflection->getName();
            }

            if (!array_key_exists('name', $asserter)) {
                $asserter['name'] = $classname;
            }

            if (!array_key_exists('methods', $asserter)) {
                $asserter['methods'] = [];
            }

            if (!array_key_exists('description', $asserter)) {
                $doc = $reflection->getDocComment();

                if ($doc) {
                    $clean = function ($v) {
                        return ltrim($v, ' *');
                    };
                    $tmp = trim(implode("\n", array_map($clean, explode("\n", substr($doc, 3, -2)))));

                    $asserter['description'] = 'It\'s the ' . lcfirst($tmp);
                }
            }

            $methods = &$data[$classname]['methods'];

            foreach ($methods as $name => &$m) {
                if (!array_key_exists('name', $m)) {
                    $m['name'] = $name;
                }

                if (!array_key_exists('inherited', $m)) {
                    $m['inherited'] = false;
                }

                if (!array_key_exists('description', $m)) {
                    $m['description'] = null;
                } elseif (!array_key_exists('signature', $m)) {
                    $m['signature'] = 'public function <<asserter>>(?string $message = null): self';
                }

                if (!array_key_exists('link', $m)) {
                    $m['link'] = null;
                }

                if (!array_key_exists('signature', $m)) {
                    $m['signature'] = null;
                }
            }

            foreach ($reflection->getMethods(ReflectionMethod::IS_PUBLIC) as $method) {
                $reference = $name = $method->getName();

                if (strpos($name, 'isNot') === 0) {
                    $reference = str_replace('isNot', 'is', $name);
                }

                if (strpos($name, 'not') === 0) {
                    $reference = lcfirst(str_replace('not', '', $name));
                }

                if (strpos($name, 'hasNot') === 0) {
                    $reference = str_replace('hasNot', 'has', $name);
                }

                if (!array_key_exists($reference, $methods)) {
                    if (VERBOSE) {
                        echo 'Skip ', $classname, '::', $name, "\n";
                    }

                    continue;
                }

                if (VERBOSE) {
                    echo 'Process ', $classname, '::', $name, "\n";
                }

                if (!array_key_exists($name, $methods)) {
                    $methods[$name] = array_merge($methods[$reference], ['description' => null, 'name' => $name]);
                }

                $methodData = &$methods[$name];

                if (!$methodData['link']) {
                    $parentName = $method->getDeclaringClass()->getName();

                    if ($parentName !== $reflectionName) {
                        $methodData['link'] = '';

                        if (strpos($parentName, $ns) === 0) {
                            $methodData['inherited'] = str_replace($ns, '', $parentName);
                        } elseif ($parentName === 'atoum\atoum\json\asserter') {
                            $methodData['inherited'] = 'asserter';
                        } else {
                            $methodData['inherited'] = str_replace('atoum\atoum\asserters\\', '', $parentName);
                            $methodData['link'] = 'https://atoum.readthedocs.io/en/latest/asserters.html';

                            if (strpos($methodData['inherited'], 'php') === 0) {
                                $methodData['inherited'] = lcfirst(substr($methodData['inherited'], 3));
                            }
                        }

                        $tmp = preg_replace('/([a-z])([A-Z])/', '\1-\2', $methodData['inherited'] . '-' . $name);

                        $methodData['link'] .= '#' . strtolower($tmp);
                    }
                }

                $methodData['data'] = $data[$classname][$reference] ?? [];

                if (!$methodData['description']) {
                    $doc = $method->getDocComment();

                    if ($doc) {
                        $clean = function (string $v) {
                            $line = ltrim($v, ' *');

                            if (str_starts_with($line, '@')) {
                                return '';
                            }

                            return $line;
                        };
                        $tmp = trim(implode("\n", array_map($clean, explode("\n", substr($doc, 3, -2)))));

                        $methodData['description'] = '`' . $name . '` ' . lcfirst($tmp);
                    }
                }

                if (!array_key_exists('files', $methodData)) {
                    $file = __DIR__ . '/json/' . $classname . '/' . $reference . '.json';

                    if (is_file($file)) {
                        $methodData['files'] = $file;
                    }
                }

                if (!$methodData['signature']) {
                    $params = [];

                    foreach ($method->getParameters() as $parameter) {
                        $type = '';
                        $default = '';

                        if ($parameter->isDefaultValueAvailable()) {
                            if ($parameter->getDefaultValue() === null) {
                                $default = '= null';
                            } else {
                                $default = sprintf('= \'%s\'', $parameter->getDefaultValue());
                            }
                        }

                        if ($parameter->getType()) {
                            $type = (string) $parameter->getType();
                        }

                        $paramName = '$' . $parameter->getName();

                        if ($paramName === '$failMessage' || $paramName === '$message') {
                            $type = '?string';
                            $paramName = '$message';
                            $default = '= null';
                        }

                        $params[] = trim($type . ' ' . $paramName . ' ' . $default);
                    }

                    $return = $method->getReturnType();
                    $returnType = null;

                    if ($return) {
                        $returnType = (string) $return;
                    }

                    if ($returnType === null && in_array($name, ['isEqualTo', 'isNotEqualTo', 'isIdenticalTo', 'isNotIdenticalTo'], true)) {
                        $returnType = 'self';
                    }

                    if ($returnType) {
                        $returnType = ': ' . $returnType;
                    }

                    $methodData['signature'] = 'public function <<asserter>>(' . implode(', ', $params) . ')' . $returnType;
                }
            }
        }
    }

    closedir($handle);
}

uksort($data, function ($a, $b) {
    return strcmp($a, $b);
});

$atoum = new atoum\atoum\json\asserters\json();
$atoum->getGenerator()->addNamespace($ns);

$file = fopen(__DIR__ . '/../README.md', 'w');

fwrite($file, "\n# " . $package['name'] . "\n\n");

if (array_key_exists('description', $package) && $package['description']) {
    fwrite($file, '> ' . $package['description'] . "\n\n");
}

fwrite($file, '[![GitLab](https://img.shields.io/static/v1?message=GitLab&logo=gitlab&color=grey&label=)](https://gitlab.com/' . $package['name'] . ")\n");
fwrite($file, '[![Latest stable version](https://img.shields.io/packagist/v/' . $package['name'] . ')](https://packagist.org/packages/' . $package['name'] . ")\n");
fwrite($file, '[![Build status](https://gitlab.com/' . $package['name'] . '/badges/main/pipeline.svg)](https://gitlab.com/' . $package['name'] . "/-/pipelines)\n");
fwrite($file, '[![Coverage status](https://img.shields.io/codecov/c/gitlab/' . $package['name'] . ')](https://codecov.io/gl/' . $package['name'] . "/)\n");
fwrite($file, '[![Minimal PHP version](https://img.shields.io/packagist/php-v/' . $package['name'] . ')](https://gitlab.com/' . $package['name'] . ")\n");
fwrite($file, '[![License](https://img.shields.io/packagist/l/' . $package['name'] . ')](https://gitlab.com/' . $package['name'] . "/-/blob/main/LICENSE)\n");

fwrite($file, "\n\n" . trim($longDescription) . "\n\n");

$blocs = [
    [
        'title' => 'Install it',
        'content' => [
            'Install extension using [composer](https://getcomposer.org):',
            [
                'code' => 'composer require --dev ' . $package['name'],
            ],
        ],
    ],
    [
        'title' => 'Use it',
        'content' => [
            'The extension is automatically added to atoum configuration.',
            'You can use it directly after installation.',
            [
                'code' => trim(file_get_contents(__DIR__ . '/example.php')),
                'lang' => 'php',
            ],
        ],
    ],
    [
        'title' => 'Asserters',
        'content' => [
            'Every asserters allow a final `string` argument to personalize error message.',
            'They are all fluent, you can chain assertions, we automatically find the better context for your asserters.',
            [
                'You should also know that all assertions without parameter can be written with or without parenthesis.',
                'So `$this->integer(0)->isZero()` is the same as `$this->integer(0)->isZero`.',
            ],
        ],
    ],
];

foreach ($blocs as $bloc) {
    fwrite($file, "\n## " . $bloc['title'] . "\n\n");

    foreach ($bloc['content'] as $line) {
        if (is_string($line)) {
            fwrite($file, $line . "\n\n");
        } elseif (array_key_exists('code', $line)) {
            fwrite($file, '```' . ($line['lang'] ?? '') . "\n" . $line['code'] . "\n```\n\n");
        } elseif (is_array($line)) {
            fwrite($file, join("\n", $line) . "\n\n");
        }
    }
}

foreach ($data as $asserter) {
    if (VERBOSE) {
        echo "\n", 'Writing ' . $asserter['name'], "\n";
    }

    fwrite($file, "\n### " . $asserter['name'] . "\n\n");

    if (array_key_exists('description', $asserter) && $asserter['description']) {
        fwrite($file, str_replace('<<assertion>>', $asserter['name'], $asserter['description']) . "\n\n");
    }

    uksort($asserter['methods'], function ($a, $b) {
        return strcmp($a, $b);
    });

    foreach ($asserter['methods'] as $method) {
        if (VERBOSE) {
            echo 'Writing ' . $asserter['name'] . '::' . $method['name'], "\n";
        }

        fwrite($file, '#### ' . $method['name'] . "\n\n");

        if (array_key_exists('description', $method) && $method['description']) {
            fwrite($file, str_replace('<<asserter>>', $method['name'], $method['description']) . "\n\n");
        }

        if (array_key_exists('signature', $method) && $method['signature']) {
            fwrite($file, "```\n" . str_replace('<<asserter>>', $method['name'], $method['signature']) . "\n```\n\n");
        }

        if ($method['inherited'] ?? false) {
            fwrite($file, <<<MD
> `{$method['name']}` is a method inherited from the `{$method['inherited']}` asserter.
For more information, refer to the documentation of [{$method['inherited']}::{$method['name']}]({$method['link']}).



MD);
        }

        if (array_key_exists('files', $method) && $method['files']) {
            $files = $method['files'];

            if (!is_array($files)) {
                $files = [
                    'json' => [
                        'path' => $method['file'],
                    ],
                ];
            }

            fwrite($file, "```php\n<?php\n\n");

            foreach ($files as $n => &$f) {
                $path = $f['path'];

                if (strpos($path, './') === 0) {
                    $path = __DIR__ . '/' . $f['path'];
                }

                $f['content'] = trim(file_get_contents($path));

                fwrite($file, '$' . $n . ' = \'' . $f['content'] . "';\n");
            }

            $method['files'] = $files;

            fwrite($file, "\n\$this\n");

            testIt($asserter['name'], $method['name'], clone $atoum, $method, $file, 4);

            fwrite($file, ";\n```\n\n");
        }
    }
}

function testIt(
    string $asserter,
    string $assertion,
    atoum\atoum\asserter $atoum,
    array $data,
    $file,
    int $indent,
    array &$lines = [],
    bool $output = true,
) {
    $quote = function ($v) use (&$quote) {
        if (is_numeric($v)) {
            return $v;
        }

        if (is_array($v)) {
            return '[' . implode(', ', array_map($quote, $v)) . ']';
        }

        if (strpos($v, '$') === 0) {
            return $v;
        }

        return '\'' . $v . '\'';
    };

    foreach ($data['calls'] as $call) {
        $name = $call['name'];
        $args = $call['args'] ?? [];

        if ($name === '<<asserter>>') {
            $name = $asserter;
        }

        if ($name === '<<assertion>>') {
            $name = $assertion;
        }

        if ($args && !is_array($args[0]) && strpos($args[0], '$') === 0) {
            $tmp = str_replace('$', '', $args[0]);

            if (array_key_exists($tmp, $data['files'])) {
                $atoum->setWith($data['files'][$tmp]['content']);
            }
        }

        $line = str_pad('', $indent) . '->' . $name . '(' . implode(', ', array_map($quote, $args)) . ')';
        $success = true;

        try {
            $atoum = $atoum->{$name}(...$args);
        } catch (atoum\atoum\asserter\exception $exc) {
            $success = false;
        }

        if (array_key_exists('calls', $call)) {
            $lines[] = [
                'text' => $line,
            ];

            testIt($asserter, $assertion, $atoum, $call, $file, $indent + 4, $lines, false);
        } else {
            $lines[] = [
                'comment' => $success ? 'succeed' : 'failed',
                'text' => $line,
            ];
        }
    }

    if ($output) {
        $max = array_reduce($lines, function ($prev, $curr) {
            return max($prev, strlen($curr['text']) + 2);
        }, 0);

        foreach ($lines as $line) {
            $comment = '';

            if (array_key_exists('comment', $line)) {
                $comment = str_pad('', $max - strlen($line['text'])) . '// ' . $line['comment'];
            }

            fwrite($file, $line['text'] . $comment . "\n");
        }
    }
}

fwrite($file, <<<LICENSE

## License

`{$package['name']}` is released under the {$package['license']} License.
See the bundled [LICENSE](LICENSE) file for details.

![atoum](http://atoum.org/images/logo/atoum.png)

LICENSE);
