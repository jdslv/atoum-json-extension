<?php // tests example

namespace my\project\tests\unit;

use atoum;

class MyClass extends atoum\atoum\test
{
    public function testJSON()
    {
        $this
            ->if($json = '<<<JSON
{
    "name": "jdslv/atoum-json-extension",
    "keywords": [
        "TDD",
        "atoum",
        "test",
        "unit testing",
        "json",
        "extension",
        "atoum-extension"
    ],
}
JSON')
            ->then
                ->json($json)
                    ->isObject
                    ->hasKeys(['keywords', 'name'])

                    ->array['keywords']
                        ->contains('atoum')
                        ->contains('json')

                    ->string['name']
                        ->isIdenticalTo('jdslv/atoum-json-extension')
        ;
    }
}
